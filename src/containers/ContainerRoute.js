import { connect } from 'react-redux'
import Router from '../router'
import { withRouter } from 'react-router-dom';

const mapStateToProps = state => {
  return {
    loggedIn: state.auth.accessToken ? true : false
  }
}


export default withRouter(connect(
  mapStateToProps
)(Router));