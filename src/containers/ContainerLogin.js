import { connect } from 'react-redux'
import Login from '../components/Login';
import { fetchAuthentication } from '../actions';

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => ({
    fetchAuthentication: user => dispatch(fetchAuthentication(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);