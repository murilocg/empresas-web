import { connect } from 'react-redux'
import CompanyList from '../components/CompanyList';
import { showEnterprise } from '../actions';

const mapStateToProps = state => {
    return {
        enterprises: formatData(state.enterprises),
        enterprise: formatDataSelectedEnterprise(state.enterprise),
        activated: state.activated
    }
}

const mapDispatchToProps = dispatch => ({
    showEnterprise: enterpriseId => dispatch(showEnterprise(enterpriseId))
})

function formatDataSelectedEnterprise(enterprise) {
    return {
        id: enterprise.id,
        description: enterprise.description
    }
}

function formatData(enterprises) {
    return enterprises.map(e => {
        return {
            id: e.id,
            name: e.enterprise_name,
            country: e.country,
            enterpriseTypeName: e.enterprise_type.enterprise_type_name
        }
    });
}


export default connect(mapStateToProps, mapDispatchToProps)(CompanyList);

export const EnterpriseListContainer = connect(mapStateToProps, mapDispatchToProps)(EnterpriseList);
export const EnterpriseContainer = connect(mapStateToProps, mapDispatchToProps)(Enterprise);
export const EmptyContainer = connect(mapStateToProps, mapDispatchToProps)(Empty)