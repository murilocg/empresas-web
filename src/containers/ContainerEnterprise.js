import { connect } from 'react-redux'
import { Enterprise } from '../components/Enterprise';
const mapStateToProps = state => {
    return {
        enterprise: formatDataSelectedEnterprise(state.enterprise),
    }
}

function formatDataSelectedEnterprise(enterprise) {
    return {
        id: enterprise.id,
        description: enterprise.description
    }
}

export default connect(mapStateToProps)(Enterprise);