import { connect } from 'react-redux'
import EnterpriseList from '../components/EnterpriseList';
import { showEnterprise } from '../actions';

const mapStateToProps = state => {
    return {
        enterprises: formatData(state.enterprises)
    }
}

const mapDispatchToProps = dispatch => ({
    showEnterprise: enterpriseId => dispatch(showEnterprise(enterpriseId))
})

function formatData(enterprises) {
    return enterprises.map(e => {
        return {
            id: e.id,
            name: e.enterprise_name,
            country: e.country,
            enterpriseTypeName: e.enterprise_type.enterprise_type_name
        }
    });
}


export default connect(mapStateToProps, mapDispatchToProps)(EnterpriseList);