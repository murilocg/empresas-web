import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import { fetchEnterprises, activateSearch } from '../actions';

const mapStateToProps = state => {
    return {
        filter: state.filter
    }
}

const mapDispatchToProps = dispatch => ({
    fetchEnterprises: value => dispatch(fetchEnterprises(value)),
    activateSearch: () => dispatch(activateSearch())
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);