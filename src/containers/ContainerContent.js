import { connect } from 'react-redux'
import { Content } from '../components/Content';
const mapStateToProps = state => {
    return {
        searched: state.activated && JSON.stringify(state.enterprise) === '{}'
    }
}

export default connect(mapStateToProps)(Content);