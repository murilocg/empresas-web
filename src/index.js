import React from 'react'
import { render } from 'react-dom'
import App from './components/App';
import './css/index.css';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers';
import { subscribeAPI } from './api'

const store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware)
);
store.subscribe(() => {
    subscribeAPI(store.getState().auth);
});

render(<Provider store={store}><App /></Provider>, document.getElementById('root'))