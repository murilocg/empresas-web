import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
import ContainerLogin from './containers/ContainerLogin';
import ContainerContent from './containers/ContainerContent'
import ContainerSearcBar from './containers/ContainerSearchBar';
const Router = ({ loggedIn }) => {
    return (
        <Switch>
            <Route exact path="/" render={() => {
                return loggedIn ? <Redirect to="/companies" /> : <ContainerLogin />
            }} />
            <Route exact path="/companies" render={() => {
                return loggedIn ? <Layout main={<ContainerContent />} /> : <Redirect to="/" />
            }} />
        </Switch>
    );
}

const Layout = ({ main }) => {
    return (
        <div id="mainLayout">
            <div id="menu" className="row">
                <ContainerSearcBar />
            </div>
            <div id="content" className="row">
                {main}
            </div>
        </div>
    )
}

Router.propTypes = {
    loggedIn: PropTypes.bool.isRequired
}

export default Router;