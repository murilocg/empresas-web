import { combineReducers } from 'redux'
import { auth } from './auth';
import { enterprises, enterprise } from './enterprise';
import { filter, activated} from './search';

const rootReducer = combineReducers({
    auth,
    enterprises,
    enterprise,
    filter,
    activated
})

export default rootReducer