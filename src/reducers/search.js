import {
    REQUEST_GET_ENTERPRISES,
    ACTIVATE_SEARCH
} from '../actions'

export const filter = (state = "", action) => {
    switch (action.type) {
        case REQUEST_GET_ENTERPRISES:
            return action.filter
        default:
            return state
    }
}

export const activated = (state = false, action) => {
    switch (action.type) {
        case ACTIVATE_SEARCH:
            return action.activated
        default:
            return state
    }
}