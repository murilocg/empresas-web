import {
    RECEIVE_AUTHENTICATION
} from '../actions'

export const auth = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_AUTHENTICATION:
            return Object.assign({}, state, action.auth)
        default:
            return state
    }
}