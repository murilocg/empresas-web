import {
    RECEIVE_GET_ENTERPRISES,
    RECEIVE_SHOW_ENTERPRISE
} from '../actions'

export const enterprises = (state = [], action) => {
    switch (action.type) {
        case RECEIVE_GET_ENTERPRISES:
            return action.enterprises
        case RECEIVE_SHOW_ENTERPRISE:
            return []
        default:
            return state
    }
}

export const enterprise = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_SHOW_ENTERPRISE:
            return Object.assign({}, state, action.enterprise)
        case RECEIVE_GET_ENTERPRISES:
            return {}
        default:
            return state
    }
}