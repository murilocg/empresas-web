import React, { Component } from 'react';
import '../css/enterpriselist.css';

class EnterpriseList extends Component {


    onClick(enterpriseId) {
        this.props.showEnterprise(enterpriseId)
    }

    render() {
        return (
            <div id="container-enterprises" className="col s12">
                <div id="enterprise-list" className={"col s12"}>
                    {
                        this.props.enterprises.map(e => {
                            return <EnterpriseItem key={e.id} enterprise={e} show={(id) => { this.onClick(id) }} />
                        })
                    }
                </div>
            </div>
        );
    }
}
class EnterpriseItem extends Component {
    render() {
        return (
            <div className="enterprise-item col s12" onClick={() => { this.props.show(this.props.enterprise.id) }}>
                <div className="col s6 m4 l3">
                    <div className="header">{"E" + this.props.enterprise.id}</div>
                </div>
                <div className="col s6 m8 l9">
                    <div className="info">
                        <div className="name">{this.props.enterprise.name}</div>
                        <div className="type">{this.props.enterprise.enterpriseTypeName}</div>
                        <div className="country">{this.props.enterprise.country}</div>
                    </div>
                </div>
            </div>
        );
    }
}
export default EnterpriseList;
