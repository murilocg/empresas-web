import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/searchbar.css';
import $ from 'jquery';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    toggle() {
        $(".brand-logo.center, #btn-search").hide("slide", () => {
            this.props.activateSearch();
            $("#search").show();
            $("#search-input").focus();
        });
    }

    onChange(value) {
        this.props.fetchEnterprises(value);
    }

    onBlur() {
        if (this.props.filter.length === 0) {
            $("#search").hide("slide", () => {
                $(".brand-logo.center, #btn-search").show();
            });
        }
    }

    clean() {
        if (this.props.filter === "") {
            this.onBlur();
        } else {
            this.onChange("");
        }
    }

    render() {
        return (
            <div className="navbar-fixed">
                <nav className="search-bar">
                    <div className="nav-wrapper">
                        <img id="logo" className="brand-logo center" alt="nav logo" src="/assets/img/logo-nav.png" />
                        <div id="btn-search" className="right">
                            <i className="material-icons" onClick={() => { this.toggle() }}>search</i>
                        </div>
                        <div id="search">
                            <div className="container-input-search">
                                <i className="material-icons" id="search-icon">search</i>
                                <input
                                    value={this.props.filter}
                                    placeholder="Pesquisar"
                                    type="text"
                                    id="search-input"
                                    onChange={(e) => { this.onChange(e.target.value) }}
                                    onBlur={() => { this.onBlur() }} autoComplete="off" />
                                <i className="material-icons" id="clean-icon" onClick={() => { this.clean() }}>close</i>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default connect()(SearchBar);
