import React, { Component } from 'react';
import ContainerEnterprise from '../containers/ContainerEnterprise';
import ContainerEnterpriseList from '../containers/ContainerEnterpriseList';

import '../css/content.css';
import $ from 'jquery';

export class Content extends Component {

  componentDidUpdate() {
    if (this.props.searched) showResults();
    else showSelected();
  }

  render() {
    return (
      <div>
        <ContainerEnterpriseList />
        <ContainerEnterprise />
        <Empty />
      </div>
    );
  }
}

const Empty = () => {
  return (
    <div id="container-empty">
      <span className="Clique-na-busca-para">Clique na busca para iniciar.</span>
    </div>
  );
}

function showResults() {
  $("#container-empty,#container-selected-enterprise").hide(() => $("#container-enterprises").show());
}

function showSelected() {
  $("#container-enterprises").hide(() => $("#container-selected-enterprise").show());
}
