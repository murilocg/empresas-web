import React, { Component } from 'react';
import '../css/enterprise.css';

export class Enterprise extends Component {

    render() {
        const name = this.props.enterprise.id ? "E" + this.props.enterprise.id : "";
        const description = this.props.enterprise.description ? this.props.enterprise.description : "";
        return (
            <div id="container-selected-enterprise">
                <div id={"selected-enterprise"}>
                    <div className="enterprise-card">
                        <div className="header">{name}</div>
                        <div className="description">
                            <p>{description}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
