import React, { Component } from 'react';
import {BrowserRouter} from  'react-router-dom'
import ContainerRoute from '../containers/ContainerRoute';

class App extends Component {
  render() {
    return(
      <BrowserRouter>
        <ContainerRoute />
      </BrowserRouter>
    );
  }
}

export default App;
