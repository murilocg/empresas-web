import React, { Component } from 'react';
import { connect } from 'react-redux'
import '../css/login.css';
class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  onChangeEmail(email) {
    this.setState({ email: email });
  }

  onChangePassword(password) {
    this.setState({ password: password })
  }

  signIn() {
    let user = { email: this.state.email, password: this.state.password };
    this.props.fetchAuthentication(user);
  }

  render() {
    return (
      <div id="container-login" className="row container-login">
        <div align="center" className="col s10 m6 l6 xl4 offset-s1 offset-m3 offset-l3 offset-xl4">
          <div className="col s10 m8 l6 offset-s1 offset-m2 offset-l3">
            <img alt="logo" src="assets/img/logo-home.png" className="responsive-img" />
          </div>
          <div className="col s10 m6 l6 xl6 offset-s1 offset-m3 offset-l3 offset-xl3 welcome">BEM VINDO AO EMPRESAS</div>
          <div className="col s12 m8 l6 xl8 offset-m2 offset-l3 offset-xl2 description">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</div>
          <div id="container-email" className="col s12 m10 l8 offset-m1 offset-l2 input-field">
            <i className="material-icons">mail_outline</i>
            <input placeholder="E-mail" id="email" type="email" className="validate" onChange={(e) => { this.onChangeEmail(e.target.value) }} />
          </div>
          <div id="container-password" className="col s12 m10 l8 offset-m1 offset-l2 input-field">
            <i className="material-icons">lock_open</i>
            <input placeholder="Senha" id="password" type="password" className="validate" onChange={(e) => { this.onChangePassword(e.target.value) }} />
          </div>
          <div className="col s12 m10 l8 offset-m1 offset-l2">
            <button id="btn-login" className="waves-effect waves-light btn" onClick={() => { this.signIn() }}>ENTRAR</button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Login);
