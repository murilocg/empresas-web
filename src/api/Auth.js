var signInHeaders = new Headers();
signInHeaders.append("Content-Type", "application/json");

export const signIn = (user, callback) => {
    var init = {
        method: 'POST',
        headers: signInHeaders,
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify({ "email": user.email, "password": user.password })
    };
    return fetch(" http://empresas.ioasys.com.br/api/v1/users/auth/sign_in", init)
        .then(response => {
            let auth = {};
            auth.accessToken = response.headers.get('access-token');
            auth.client = response.headers.get('client');
            auth.uid = response.headers.get('uid');
            return auth;
        }, error => console.log('An error occurred.', error))
        .then(callback);
}