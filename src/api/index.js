import { signIn } from './Auth';
import { showEnterprise, filterEnterprises } from './Enterprise';

var auth;

export const EntrepriseAPI = {
    signIn,
    showEnterprise: (enterpriseId, callback) => { showEnterprise(enterpriseId, auth, callback) },
    filterEnterprises: (name, enterpriseType, callback) => filterEnterprises(name, enterpriseType, auth, callback),
}

export const subscribeAPI = (value) => {
    auth = value;
}