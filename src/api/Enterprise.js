const URL = "http://empresas.ioasys.com.br/api/v1/enterprises";

export const showEnterprise = (enterpriseId, auth, callback) => {

    var init = {
        method: 'GET',
        headers: headers(auth),
        mode: 'cors',
        cache: 'default',
    };
    return fetch(`${URL}/${enterpriseId}`, init)
        .then(response => response.json(), error => console.log('An error occurred.', error))
        .then(json => callback(json.enterprise));
}

export const filterEnterprises = (name, enterpriseType, auth, callback) => {
    var init = {
        method: 'GET',
        headers: headers(auth),
        mode: 'cors',
    };
    let url = URL + "?";
    if (name) url += "name=" + name + "&";
    if (enterpriseType) url += "enterprise_types=" + enterpriseType;
    return fetch(url, init)
        .then(response => response.json(), error => console.log('An error occurred.', error))
        .then(json => callback(json.enterprises));
}

const headers = (auth) => {
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("access-token", auth.accessToken);
    headers.append("client", auth.client);
    headers.append("uid", auth.uid);
    return headers;
}