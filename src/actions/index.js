import { EntrepriseAPI } from '../api'

export const REQUEST_AUTHENTICATION = 'REQUEST_AUTHENTICATION';
export const requestAuthetication = (auth) => {
  return {
    type: REQUEST_AUTHENTICATION,
    auth
  }
}

export const RECEIVE_AUTHENTICATION = 'RECEIVE_AUTHENTICATION'
export const receiveAuthentication = (auth) => {
  return {
    type: RECEIVE_AUTHENTICATION,
    auth,
  }
}

export const fetchAuthentication = (user) => {
  return (dispatch) => {
    dispatch(requestAuthetication(user));
    return EntrepriseAPI.signIn(user, json => {
      dispatch(receiveAuthentication(json));
    })
  }
}

export const REQUEST_GET_ENTERPRISES = 'REQUEST_GET_ENTERPRISES';
export const requestGetEnterprises = (filter) => {
  return {
    type: REQUEST_GET_ENTERPRISES,
    filter: filter
  }
}

export const RECEIVE_GET_ENTERPRISES = 'RECEIVE_GET_ENTERPRISES'
export const receiveGetEnterprises = (json) => {
  return {
    type: RECEIVE_GET_ENTERPRISES,
    enterprises: json,
  }
}

export const fetchEnterprises = (filter) => {
  return (dispatch) => {
    dispatch(requestGetEnterprises(filter));
    let f = extractNameAndType(filter);
    EntrepriseAPI.filterEnterprises(f.name, f.type, json => {
      dispatch(receiveGetEnterprises(json))
    })
  }
}

export const REQUEST_SHOW_ENTERPRISE = 'REQUEST_SHOW_ENTERPRISE';
export const requestShowEnterprise = (enterpriseId) => {
  return {
    type: REQUEST_SHOW_ENTERPRISE,
    enterpriseId
  }
}

export const RECEIVE_SHOW_ENTERPRISE = 'RECEIVE_SHOW_ENTERPRISE'
export const receiveShowEnterprise = (json) => {
  return {
    type: RECEIVE_SHOW_ENTERPRISE,
    enterprise: json,
  }
}

export const showEnterprise = (enterpriseId) => {
  return (dispatch) => {
    dispatch(requestShowEnterprise(enterpriseId));
    EntrepriseAPI.showEnterprise(enterpriseId, json => {
      dispatch(receiveShowEnterprise(json))
    })
  }
}

export const ACTIVATE_SEARCH = "ACTIVATE_SEARCH";
export const activateSearch = () => {
  return {
    type: ACTIVATE_SEARCH,
    activated: true
  };
}

function extractNameAndType(filter) {
  if (!filter || filter === '') return {};
  let words = filter.split(" ");
  if (isNaN(words[words.length - 1])) return { name: filter };
  if (words.length === 1) return { type: words[0] };
  return {
    type: words[words.length - 1],
    name: words.slice(0, words.length - 1).join()
  }
}